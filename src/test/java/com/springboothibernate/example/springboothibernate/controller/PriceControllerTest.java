package com.springboothibernate.example.springboothibernate.controller;

import com.springboothibernate.example.springboothibernate.model.Price;
import com.springboothibernate.example.springboothibernate.repository.PriceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.mockito.Mockito.verify;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
class PriceControllerTest {
    @Mock
    private PriceRepository mockPriceRepository;
    @InjectMocks
    private PriceController testPriceController;

    @BeforeAll
    public void SetUp() {
        testPriceController = new PriceController();
    }
    @Test
    void TestGetDealByMerchantId_Success() {
        Price mockPrice = new Price(1, 1, 1, 1);
        Mockito.when(mockPriceRepository.findById((long)1)).thenReturn(Optional.of(mockPrice));

        ResponseEntity<Price> result = testPriceController.getDealByMerchantId(1);

        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertEquals(mockPrice, result.getBody());
        verify(mockPriceRepository).findById((long)1);
    }

    @Test
    void TestGetDealByMerchantId_Empty(){
        Mockito.when(mockPriceRepository.findById((long)1)).thenReturn(Optional.ofNullable(null));

        ResponseEntity<Price> result = testPriceController.getDealByMerchantId(1);

        Assertions.assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
        verify(mockPriceRepository).findById((long)1);
    }

    @Test
    void TestGetealByMerchantId_Error(){
        Mockito.when(mockPriceRepository.findById((long)1)).thenThrow(new IndexOutOfBoundsException());

        ResponseEntity<Price> result = testPriceController.getDealByMerchantId(1);

        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
        verify(mockPriceRepository).findById((long)1);
    }

    @Test
    void CreatePrice_Success() {
        Price mockPrice = new Price(1, 1, 1, 1);
        Mockito.when(mockPriceRepository.save(mockPrice)).thenReturn(mockPrice);

        ResponseEntity<Price> result = testPriceController.createPrice(mockPrice);

        Assertions.assertEquals(HttpStatus.CREATED, result.getStatusCode());
        Assertions.assertEquals(mockPrice, result.getBody());
        verify(mockPriceRepository).save(mockPrice);
    }

    @Test
    void CreatePrice_Error() {
        Price mockPrice = new Price(1, 1, 1, 1);
        Mockito.when(mockPriceRepository.save(mockPrice)).thenThrow(new IndexOutOfBoundsException());

        ResponseEntity<Price> result = testPriceController.createPrice(mockPrice);

        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
        verify(mockPriceRepository).save(mockPrice);
    }

    @Test
    void DeletePrice_Success() {
        ResponseEntity<HttpStatus> result = testPriceController.deletePrice(1);

        Assertions.assertEquals(HttpStatus.NO_CONTENT, result.getStatusCode());
        verify(mockPriceRepository).deleteById((long)1);
    }

    @Test
    void DeletePrice_Error(){
        Mockito.doThrow(new IndexOutOfBoundsException()).when(mockPriceRepository).deleteById((long)1);

        ResponseEntity<HttpStatus> result = testPriceController.deletePrice(1);

        Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
        verify(mockPriceRepository).deleteById((long)1);
    }
}