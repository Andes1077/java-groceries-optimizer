package com.springboothibernate.example.springboothibernate.controller;

import com.springboothibernate.example.springboothibernate.model.Price;
import com.springboothibernate.example.springboothibernate.repository.PriceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/prices")
public class PriceController {
    @Autowired
    PriceRepository priceRepository;

    @GetMapping("/merchant/{mid}")
    public ResponseEntity<Price> getDealByMerchantId(@PathVariable("mid") long mid) {
        try{
            Optional<Price> price = priceRepository.findById(mid);
            if (price.isPresent()) {
                return new ResponseEntity<>(price.get(), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("")
    public ResponseEntity<Price> createPrice(@RequestBody Price price){
        try{
            Price _price = priceRepository.save(price);
            return new ResponseEntity<>(_price, HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/merchant/{mid}")
    public ResponseEntity<HttpStatus> deletePrice(@PathVariable("mid") long mid){
        try {
            priceRepository.deleteById(mid);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
