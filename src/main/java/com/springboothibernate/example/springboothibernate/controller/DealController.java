package com.springboothibernate.example.springboothibernate.controller;

import java.util.List;
import java.util.Map;

import com.springboothibernate.example.springboothibernate.model.Deal;
import com.springboothibernate.example.springboothibernate.service.DealService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/deals")
public class DealController {
    @Autowired
    DealService dealService;

    @PostMapping("merchant/{mid}/best")
    public ResponseEntity<Integer> getBestPriceByMerchantId(@PathVariable("mid")  long mid, @RequestBody Map<String, List<Integer>> needs){
        try {
            return new ResponseEntity<Integer>(Integer.valueOf(dealService.findBest(mid, needs.get("needs"))), HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/merchant/{mid}")
    public ResponseEntity<List<Deal>> getDealByMerchantId(@PathVariable("mid") long mid) {
        try {
            List<Deal> deals = dealService.findByMid(mid);

            if (deals.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(deals, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/merchant/{mid}/deal/{did}")
    public ResponseEntity<Deal> createTutorial(@PathVariable("mid") long mid, @PathVariable("did") long did, @RequestBody Deal deal) {
        try {
            Deal _deal = dealService.save(mid, did, deal);
            return new ResponseEntity<>(_deal, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/merchant/{mid}/deal/{did}")
    public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("mid") long mid, @PathVariable("mid") long did) {
        try {
            dealService.deleteByMidAndDid(mid, did);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}