package com.springboothibernate.example.springboothibernate.repository;

import com.springboothibernate.example.springboothibernate.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PriceRepository extends JpaRepository<Price, Long> {
}
