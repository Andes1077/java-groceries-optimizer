package com.springboothibernate.example.springboothibernate.repository;

import com.springboothibernate.example.springboothibernate.model.Deal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DealRepository extends JpaRepository<Deal, Long> {
    List<Deal> findByMid(long mid);
    @Transactional

    void deleteByMidAndDid(long mid, long did);
}
