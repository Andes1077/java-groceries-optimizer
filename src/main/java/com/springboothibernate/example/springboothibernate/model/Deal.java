package com.springboothibernate.example.springboothibernate.model;

import jakarta.persistence.*;
import com.springboothibernate.example.springboothibernate.model.CompositeKey;

@Entity
@IdClass(CompositeKey.class)
@Table(name = "deals")
public class Deal {
    public int getMid() {
        return mid;
    }

    public int getDid() {
        return did;
    }

    public int getMuffin() {
        return muffin;
    }

    public int getYogurt() {
        return yogurt;
    }

    public int getOnion() {
        return onion;
    }

    public int getPrice() {
        return price;
    }

    @Id
    private int mid;
    @Id
    private int did;

    @Column(name = "muffin")
    private int muffin;

    @Column(name = "yogurt")
    private int yogurt;

    @Column(name = "onion")
    private int onion;

    @Column(name = "price")
    private int price;

    public Deal() {
    }

    public Deal(int mid, int did, int muffin, int yogurt, int onion, int price) {
        this.mid = mid;
        this.did = did;
        this.muffin = muffin;
        this.yogurt = yogurt;
        this.onion = onion;
        this.price = price;
    }
}
