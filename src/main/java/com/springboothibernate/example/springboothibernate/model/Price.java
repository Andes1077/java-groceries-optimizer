package com.springboothibernate.example.springboothibernate.model;

import jakarta.persistence.*;

@Entity
@Table(name = "prices")
public class Price {
    public int getMid() {
        return mid;
    }

    public int getMuffin() {
        return muffin;
    }

    public int getYogurt() {
        return yogurt;
    }

    public int getOnion() {
        return onion;
    }

    @Id
    private int mid;
    @Column(name = "muffin")
    private int muffin;

    @Column(name = "yogurt")
    private int yogurt;

    @Column(name = "onion")
    private int onion;

    public Price(){
    }

    public Price(int mid, int muffin, int yogurt, int onion){
        this.mid = mid;
        this.muffin = muffin;
        this.yogurt = yogurt;
        this.onion = onion;
    }
}
