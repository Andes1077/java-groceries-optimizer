package com.springboothibernate.example.springboothibernate.service;

import com.springboothibernate.example.springboothibernate.model.Deal;
import com.springboothibernate.example.springboothibernate.model.Price;
import com.springboothibernate.example.springboothibernate.repository.DealRepository;
import com.springboothibernate.example.springboothibernate.repository.PriceRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class DealService{
    @Autowired
    DealRepository dealRepository;
    @Autowired
    PriceRepository priceRepository;

    public List<Deal> findByMid(long mid){
        return dealRepository.findByMid(mid);
    }

    public Deal save(long mid, long did, Deal deal){
        return dealRepository.save(new Deal((int)mid, (int)did, deal.getMuffin(), deal.getYogurt(), deal.getOnion(), deal.getPrice()));
    }

    public void deleteByMidAndDid(long mid, long did){
        dealRepository.deleteByMidAndDid(mid, did);
    }

    public int findBest(long mid, List<Integer> needs){
        List<Deal> deals = findByMid(mid);
        Price price = priceRepository.findById(mid).get();

        List<Integer> priceList = priceToList(price);
        List<List<Integer>> dealsList = dealsToList(deals);

        return shoppingOffers(priceList, dealsList, needs);
    }

    private List<Integer> priceToList(Price price){
        List<Integer> ans = new ArrayList<Integer>();
        ans.add(price.getMuffin());
        ans.add(price.getYogurt());
        ans.add(price.getOnion());
        return ans;
    }
    private List<List<Integer>> dealsToList(List<Deal> deals){
        List<List<Integer>> ans = new ArrayList<List<Integer>>();
        for (Deal deal: deals){
            ArrayList<Integer> temp = new ArrayList<Integer>();
            temp.add(deal.getMuffin());
            temp.add(deal.getYogurt());
            temp.add(deal.getOnion());
            temp.add(deal.getPrice());
            ans.add(temp);
        }
        return ans;
    }
    private int shoppingOffers(List<Integer> price, List<List<Integer>> special, List<Integer> needs) {
        return helper(price, special, needs, 0);
    }

    private int helper(List<Integer> price, List<List<Integer>> special, List<Integer> needs, int pos) {
        int sum = 0, len = price.size();
        for (int i = 0; i < len; i++) sum += needs.get(i)*price.get(i);

        for (int i = pos; i < special.size(); i++) {
            List<Integer> list = special.get(i);
            List<Integer> temp = new ArrayList<>();
            for (int j= 0; j < needs.size(); j++) {
                if (needs.get(j) < list.get(j)) { // check if the current offer is valid
                    temp =  null;
                    break;
                }
                temp.add(needs.get(j) - list.get(j));
            }
            if (temp != null) { // use the current offer and try next
                sum = Math.min(sum, list.get(list.size() - 1) + helper(price, special, temp, i));
            }
        }
        return sum;
    }
}