FROM maven:3.8.3-openjdk-17
EXPOSE 8080
COPY pom.xml /usr/local/service/pom.xml
COPY logback.xml /usr/local/service/logback.xml
COPY src /usr/local/service/src
WORKDIR /usr/local/service
RUN mvn package -Dmaven.test.skip
ENTRYPOINT ["java", "-jar", "-Dlogging.config=/usr/local/service/logback.xml", "/usr/local/service/target/spring-boot-docker.jar"]