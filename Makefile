start:
	docker-compose up -d --no-deps --build web
	docker-compose up -d

down:
	docker-compose down

restart:
	make down
	make start

log:
	docker logs --follow springbootcontainer

exec:
	docker exec -it springbootcontainer /bin/sh