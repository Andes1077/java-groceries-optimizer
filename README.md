# Overview
Doing groceries is hard. Information come and go without our notice. Actionable intelligence will almost always turn out to be in vain under our scarce mental capacity. 

We understand the pain of being left out of the loop of great deals. We also understand how helpless one can be in the face of good deals and limited cognitive function that prevents us from acting upon it.

That is why we are here to proudly present you our latest solution--JOp, also pronounced as J-Op.

It gathers bundles of a basket of goods that frequent CPI, aka Consumer Product Index, and a myriad of global providers that come with plenty of deals. With only the following 2 information,
* Which provider do you want your deals from?
* How many goods you want respectively from that basket?

we promise you the lowest cost just in a heartbeat.

Scroll down for more details!

# Mermaid Diagram

```mermaid
sequenceDiagram
    participant Global Provider
    participant JOp
    participant You
    Global Provider->>JOp: GET, POST, DELETE to update their bundle of deals
    JOp->>You: Lowest cost to pay your global and local provider
    You->>JOp: POST Merchant ID, Needs, Price
```

# Technologies Stack
* Spring Boot
* Postgres
* Docker
* GitLab CI/CD

# Implementation
JOp follows the principle of Service-repository pattern under a fully dockerized Spring Boot and Postgres framework. It is also developed under Test-driven environment with continuous integration with GitLab. Structured log, along with docker log and Makefile, is added to ease development process.

# If you are a provider...
Register how much do you charge for individual goods through our JOp API, for example,

| Muffin | Yogurt | Onion |
|--------|--------|--------|
| $3     | $2     | $3|

Also register the content of each bundle and their associated price, for example,

| Bundle / Quantity | Muffin | Yogurt | Onion | Price |
|---|---|---|---|---|
| A | 3 | 2 | 1 | 10|
| B | 1 | 5 | 2 | 20|
| C | 5 | 2 | 4 | 50|

# If you are a customer...
Tell us how many of goods do you need through our JOp API, for example,

| |Muffin | Yogurt |Onion|
|---|---|---|---|
|Quantity| 1 | 3 | 2|